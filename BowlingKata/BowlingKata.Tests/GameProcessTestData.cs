﻿using System.Collections;
using System.Collections.Generic;

namespace BowlingKata.Tests
{
    class GameProcessTestData : IEnumerable<object[]>
    {
        public IEnumerator<object[]> GetEnumerator()
        {
            //first roll
            yield return new object[] {5, 5 };
            yield return new object[] {2, 2};
            //second roll
            yield return new object[] {8, 3, 5 };
            yield return new object[] {10, 4, 6 };
            //strike
            yield return new object[] {28, 10, 6, 3 };
            //spare
            yield return new object[] {22, 3, 7, 6 };
            //game process
            yield return new object[] {61, 1, 4, 4, 5, 6, 4, 5, 5, 10, 0, 1 };
            yield return new object[] {71, 1, 4, 4, 5, 6, 4, 5, 5, 10, 0, 1, 7, 3 };
            yield return new object[] {107, 1, 4, 4, 5, 6, 4, 5, 5, 10, 0, 1, 7, 3, 6, 4, 10 };
            yield return new object[] {133, 1, 4, 4, 5, 6, 4, 5, 5, 10, 0, 1, 7, 3, 6, 4, 10, 2, 8, 6 };
            //10 frame; first roll 10
            yield return new object[] {144, 1, 4, 4, 5, 6, 4, 5, 5, 10, 0, 1, 7, 3, 6, 4, 10, 10, 8, 1 };
            yield return new object[] {145, 1, 4, 4, 5, 6, 4, 5, 5, 10, 0, 1, 7, 3, 6, 4, 10, 10, 8, 2 };
        }

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}
