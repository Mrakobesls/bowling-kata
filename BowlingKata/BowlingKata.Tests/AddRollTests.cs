﻿using System;
using Xunit;
using Xunit.Abstractions;

namespace BowlingKata.Tests
{
    public class AddRollTests
    {
        private readonly BowlingKataGame _game;
        public AddRollTests()
        {
            _game = new BowlingKataGame();
        }

        [Theory]
        [InlineData(14)]
        [InlineData(-3)]
        public void AddFirstRoll_ThrowArgumentExctprion_IfIncorrectlyAdded(int points)
        {
            //Arrange
            //Act
            Action act = () => _game.MakeRoll(points);
            //Assert
            Assert.Throws<ArgumentException>(act);
        }

        [Theory]
        [InlineData(9, 2)]
        [InlineData(5, 6)]
        public void AddSecondRoll_ThrowArgumentExctprion_IfFrameValueIsMoreThan10(params int[] points)
        {
            //Arrange
            //Act
            _game.MakeRoll(points[0]);
            Action act = () => _game.MakeRoll(points[1]);
            //Assert
            Assert.Throws<ArgumentException>(act);
        }
        
        [Theory]
        [ClassData(typeof(GameProcessTestData))]
        public void CountingFrames_True_IfCorrectlyCounted(int expected, params int[] points)
        {
            //Arrange
            //Act
            foreach (var item in points)
            {
                _game.MakeRoll(item);
            }

            var actual = _game.GetCurrentScore();
            //Assert
            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData(1, 4, 4, 5, 6, 4, 5, 5, 10, 0, 1, 7, 3, 6, 4, 10, 10, 8, 5)]
        public void LastFrameFirstRollStrike_ThrowArgumentExctprion_IfWrongAmountOfPoints(params int[] points)
        {
            //Arrange
            //Act
            for (var i = 0; i < points.Length - 1; i++)
            {
                _game.MakeRoll(points[i]);
            }
            Action act = () => _game.MakeRoll(points[1]);
            //Assert
            Assert.Throws<ArgumentException>(act);
        }

        [Theory]
        [InlineData(1, 4, 4, 5, 6, 4, 5, 5, 10, 0, 1, 7, 3, 6, 4, 10, 2, 3, 6)]
        public void BonusRollException_ThrowException_IfNotBonusedRoll(params int[] points)
        {
            //Arrange
            //Act
            for (var i = 0; i < points.Length - 1; i++)
            {
                _game.MakeRoll(points[i]);
            }
            Action act = () => _game.MakeRoll(points[^1]);
            //Assert
            Assert.Throws<Exception>(act);
        }

        [Theory]
        [InlineData(1, 4, 4, 5, 6, 4, 5, 5, 10, 0, 1, 7, 3, 6, 4, 10, 2, 8, 6, 5)]
        public void RollAfterBonusRollException_ThrowException_IfExcessRoll(params int[] points)
        {
            //Arrange
            //Act
            for (var i = 0; i < points.Length - 1; i++)
            {
                _game.MakeRoll(points[i]);
            }
            Action act = () => _game.MakeRoll(points[^1]);
            //Assert
            Assert.Throws<Exception>(act);
        }
    }
}
