﻿using System;

namespace BowlingKata
{
    public class BowlingKataGame
    {
        private readonly Frame[] _frames = new Frame[10];
        private readonly Bonus[] _bonuses = { Bonus.Empty, Bonus.Empty };
        private int _bonusRoll = 0;
        private int _currentFrame = 0;
        private bool _isTheGameOver = false;
        public BowlingKataGame()
        {
            for (var i = 0; i < _frames.Length; i++)
            {
                _frames[i] = new Frame();
            }
        }

        public void MakeRoll(int points)
        {
            if (_isTheGameOver)
                throw new Exception("The game ended");
            if (!IsCorrectRollValue())
            {
                throw new ArgumentException();
            }
            if (!IsCorrectNinthFrameValues())
            {
                throw new ArgumentException();
            }
            if (IsBonusRoll())
            {
                _bonusRoll = points;
                _isTheGameOver = true;
                return;
            }
            if (IsFirstRoll())
            {
                _frames[_currentFrame].FirstRoll = points;
                if (IsStrike())
                {
                    AddBonus(Bonus.FullStrike);
                    if (IsNinthFrame())
                    {
                        if (_frames[9].FirstRoll != 10 && (_frames[9].FirstRoll + _frames[9].SecondRoll != 10))
                            _isTheGameOver = true;
                        return;
                    }
                    _frames[_currentFrame].SecondRoll = 0;
                    ++_currentFrame;
                    _frames[_currentFrame].CurrentFrameScore = _frames[_currentFrame - 1].CurrentFrameScore;
                }
                else
                    AddBonus(Bonus.Empty);
            }
            else
            {
                _frames[_currentFrame].SecondRoll = points;
                AddBonus((_frames[_currentFrame].FirstRoll + _frames[_currentFrame].SecondRoll) == 10
                    ? Bonus.Spare
                    : Bonus.Empty);

                if (IsNinthFrame())
                {
                    if (!IsAnAllowedBonusRole())
                        _isTheGameOver = true;
                    return;
                }
                ++_currentFrame;
                _frames[_currentFrame].CurrentFrameScore = _frames[_currentFrame - 1].CurrentFrameScore;
            }

            bool IsCorrectRollValue()
            {
                return points >= 0 && points <= 10;
            }
            bool IsCorrectNinthFrameValues()
            {
                return !((_frames[_currentFrame].FirstRoll != 10 && _frames[_currentFrame].FirstRoll + points > 10) ||
                       (_frames[_currentFrame].FirstRoll == 10 && _frames[_currentFrame].SecondRoll != 10 &&
                        _frames[_currentFrame].SecondRoll + points > 10));
            }
            bool IsAnAllowedBonusRole()
            {
                return _frames[_currentFrame].FirstRoll == 10 || (_frames[9].FirstRoll + _frames[_currentFrame].SecondRoll == 10);
            }
            bool IsBonusRoll()
            {
                return _frames[9].FirstRoll != null && _frames[9].SecondRoll != null;
            }
            bool IsStrike()
            {
                return _frames[_currentFrame].FirstRoll == 10;
            }
            bool IsNinthFrame()
            {
                return _currentFrame == 9;
            }
        }

        public int GetCurrentScore()
        {
            return _frames[_currentFrame].CurrentFrameScore.GetValueOrDefault() + _bonusRoll;
        }
        private void AddBonus(Bonus bonus)
        {
            AddBonusPoints();
            _bonuses[0] =
                _currentFrame != 9
                ? bonus
                : Bonus.Empty;
        }
        private void AddBonusPoints()
        {
            if (IsThereHalfStrike())
            {
                if (IsFirstRoll())
                {
                    _frames[_currentFrame - 2].CurrentFrameScore += _frames[_currentFrame].FirstRoll;
                    _frames[_currentFrame - 1].CurrentFrameScore += _frames[_currentFrame].FirstRoll;
                    _frames[_currentFrame].CurrentFrameScore += _frames[_currentFrame].FirstRoll;
                }
                else
                {
                    _frames[_currentFrame - 1].CurrentFrameScore += _frames[_currentFrame].SecondRoll;
                    _frames[_currentFrame].CurrentFrameScore += _frames[_currentFrame].SecondRoll;
                }

                _bonuses[1] = Bonus.Empty;
            }

            if (IsTheLastBonusAddedNotFullStrikeOrSpare()) return;
            _frames[_currentFrame - 1].CurrentFrameScore += _frames[_currentFrame].FirstRoll;
            _frames[_currentFrame].CurrentFrameScore += _frames[_currentFrame].FirstRoll;
            if (WasFullStrike())
                _bonuses[1] = Bonus.HalfStrike;


            bool IsThereHalfStrike()
            {
                return _bonuses[1] == Bonus.HalfStrike;
            }
            bool IsTheLastBonusAddedNotFullStrikeOrSpare()
            {
                return _bonuses[0] != Bonus.FullStrike && _bonuses[0] != Bonus.Spare;
            }
            bool WasFullStrike()
            {
                return _bonuses[0] == Bonus.FullStrike;
            }
        }
        private bool IsFirstRoll()
        {
            return _frames[_currentFrame].FirstRoll == null;
        }
        public enum Bonus
        {
            Empty = 0,
            Spare = 1,
            FullStrike = 2,
            HalfStrike = 3
        }
    }
}
