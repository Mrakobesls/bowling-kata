﻿namespace BowlingKata
{
    internal class Frame
    {
        private int? _firstRoll;
        public int? FirstRoll
        {
            get => _firstRoll;
            set
            {
                _firstRoll = value;
                CurrentFrameScore += value;
            }
        }
        private int? _secondRoll;
        public int? SecondRoll
        {
            get => _secondRoll;
            set
            {
                _secondRoll = value;
                CurrentFrameScore += value;
            }
        }
        public int? CurrentFrameScore { get; set; }

        public Frame()
        {
            _firstRoll = null;
            _secondRoll = null;
            CurrentFrameScore = 0;
        }
    }
}
