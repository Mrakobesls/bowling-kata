﻿using System;
using System.Runtime.Versioning;
using BowlingKata;

namespace Program
{
    internal class Program
    {
        private static void Main()
        {
            var gameProcess = new [] {1, 4, 4, 5, 6, 4, 5, 5, 10, 0, 1, 7, 3, 6, 4, 10, 10, 8, 2};
            var game = new BowlingKataGame();
            foreach (var item in gameProcess)
            {
                game.MakeRoll(item);
            }
            Console.WriteLine(game.GetCurrentScore());
        }
    }
}
