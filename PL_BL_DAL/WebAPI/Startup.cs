using DataBase;
using DataBase.Model;
using FluentValidation;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Repository;
using Repository.Interfaces;
using Services.DTO;
using Services.Mapping;
using Services.Services;
using Services.Services.Intefaces;
using Services.Validators;

namespace WebAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }
        
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            services.AddDbContext<DbContext, BaseDbContext>();

            services.AddScoped<IGenericRepository<Card>, GenericRepository<Card>>();
            services.AddScoped<IGenericRepository<Board>, GenericRepository<Board>>();
            services.AddScoped<IGenericRepository<Column>, GenericRepository<Column>>();
            services.AddScoped<IGenericRepository<Comment>, GenericRepository<Comment>>();
            services.AddScoped<IGenericRepository<Label>, GenericRepository<Label>>();
            services.AddScoped<IGenericRepository<Permission>, GenericRepository<Permission>>();
            services.AddScoped<IGenericRepository<User>, GenericRepository<User>>();

            services.AddScoped<IService<BoardDto>, BoardService>();
            services.AddScoped<IService<CardDto>, CardService>();
            services.AddScoped<IService<ColumnDto>, ColumnService>();
            services.AddScoped<IService<CommentDto>, CommentService>();
            services.AddScoped<IService<LabelDto>, LabelService>();
            services.AddScoped<IService<PermissionDto>, PermissionService>();
            services.AddScoped<IService<UserDto>, UserService>();
            
            services.AddAutoMapper(typeof(MappingProfile).Assembly);
            services.AddScoped<IValidator<BoardDto>, BoardValidator>();
            services.AddScoped<IValidator<CardDto>, CardValidator>();
            services.AddScoped<IValidator<ColumnDto>, ColumnValidator>();
            services.AddScoped<IValidator<CommentDto>, CommentValidator>();
            services.AddScoped<IValidator<LabelDto>, LabelValidator>();
            services.AddScoped<IValidator<PermissionDto>, PermissionValidator>();
            services.AddScoped<IValidator<UserDto>, UserValidator>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
