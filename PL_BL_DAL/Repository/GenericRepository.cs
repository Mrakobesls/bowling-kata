﻿using System.Linq;
using DataBase;
using Microsoft.EntityFrameworkCore;
using Repository.Interfaces;

namespace Repository
{
    public class GenericRepository<T> : IGenericRepository<T>
        where T : class
    {
        private readonly BaseDbContext _context;
        public GenericRepository(DbContext context)
        {
            _context = (BaseDbContext)context;
        }
        public void Create(T entity)
        {
            _context.Add(entity);
        }
        public T Read(int id)
        {
            return _context.Find<T>(id);
        }
        public IQueryable<T> ReadAll()
        {
            return _context.Set<T>();
        }
        public T Update(T entity)
        {
            return _context.Update(entity).Entity;
        }
        public virtual void Delete(T entity)
        {
            _context.Remove(entity);
        }
    }
}
