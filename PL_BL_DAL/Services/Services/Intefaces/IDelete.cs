﻿namespace Services.Services.Intefaces
{
    public interface IDelete<T>
        where T : class
    {
        void Delete(T instance);
    }
}
