﻿namespace Services.Services.Intefaces
{
    public interface IUpdate<T>
        where T : class
    {
        T Update(T instance);
    }
}
