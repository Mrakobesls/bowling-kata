﻿using System;
using System.Linq;
using AutoMapper;
using DataBase.Model;
using FluentValidation;
using Repository.Interfaces;
using Services.DTO;
using Services.Services.Intefaces;

namespace Services.Services
{
    public class ColumnService : IService<ColumnDto>
    {
        private readonly IGenericRepository<Column> _repository;
        private readonly IMapper _mapper;
        private readonly IValidator<ColumnDto> _validator;
        public ColumnService(IGenericRepository<Column> repository, IMapper mapper, IValidator<ColumnDto> validator)
        {
            _repository = repository;
            _mapper = mapper;
            _validator = validator;
        }
        public void Create(ColumnDto instance)
        {
            Validate(instance);
            _repository.Create(_mapper.Map<Column>(instance));
        }
        public ColumnDto Read(int id)
        {
            var temp = _repository.Read(id);
            return _mapper.Map<ColumnDto>(temp);
        }
        public IQueryable<ColumnDto> ReadAll()
        {
            return _repository
                .ReadAll()
                .Select(b => _mapper.Map<ColumnDto>(b));
        }
        public ColumnDto Update(ColumnDto instance)
        {
            Validate(instance);
            return _mapper.Map<ColumnDto>(_repository
                .Update(_mapper.Map<Column>(instance)));
        }
        public virtual void Delete(ColumnDto instance)
        {
            Validate(instance);
            _repository.Delete(_mapper.Map<Column>(instance));
        }

        public void Validate(ColumnDto instance)
        {
            var validity = _validator.Validate(instance);
            if (!validity.IsValid)
                throw new ArgumentOutOfRangeException(validity.ToString());
        }
    }
}
