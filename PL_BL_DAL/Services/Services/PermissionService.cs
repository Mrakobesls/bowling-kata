﻿using System;
using System.Linq;
using AutoMapper;
using DataBase.Model;
using FluentValidation;
using Repository.Interfaces;
using Services.DTO;
using Services.Services.Intefaces;

namespace Services.Services
{
    public class PermissionService : IService<PermissionDto>
    {
        private readonly IGenericRepository<Permission> _repository;
        private readonly IMapper _mapper;
        private readonly IValidator<PermissionDto> _validator;
        public PermissionService(IGenericRepository<Permission> repository, IMapper mapper, IValidator<PermissionDto> validator)
        {
            _repository = repository;
            _mapper = mapper;
            _validator = validator;
        }
        public void Create(PermissionDto instance)
        {
            Validate(instance);
            _repository.Create(_mapper.Map<Permission>(instance));
        }
        public PermissionDto Read(int id)
        {
            var temp = _repository.Read(id);
            return _mapper.Map<PermissionDto>(temp);
        }
        public IQueryable<PermissionDto> ReadAll()
        {
            return _repository
                .ReadAll()
                .Select(b => _mapper.Map<PermissionDto>(b));
        }
        public PermissionDto Update(PermissionDto instance)
        {
            Validate(instance);
            return _mapper.Map<PermissionDto>(_repository
                .Update(_mapper.Map<Permission>(instance)));
        }
        public virtual void Delete(PermissionDto instance)
        {
            Validate(instance);
            _repository.Delete(_mapper.Map<Permission>(instance));
        }

        public void Validate(PermissionDto instance)
        {
            var validity = _validator.Validate(instance);
            if (!validity.IsValid)
                throw new ArgumentOutOfRangeException(validity.ToString());
        }
    }
}
