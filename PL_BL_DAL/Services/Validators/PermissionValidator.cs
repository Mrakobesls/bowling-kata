﻿using FluentValidation;
using Services.DTO;

namespace Services.Validators
{
    public class PermissionValidator : AbstractValidator<PermissionDto>
    {
        public PermissionValidator()
        {
            RuleFor(p => p.Id).NotNull().GreaterThan(0);
            RuleFor(p => p.Name).NotNull().MaximumLength(30);
        }
    }
}
