﻿using FluentValidation;
using Services.DTO;

namespace Services.Validators
{
    public class ColumnValidator : AbstractValidator<ColumnDto>
    {
        public ColumnValidator()
        {
            RuleFor(c => c.Id).NotNull().GreaterThan(0);
            RuleFor(c => c.BoardId).NotNull().GreaterThan(0);
            RuleFor(b => b.Title).NotNull().MaximumLength(20);
        }
    }
}
