﻿using System.Collections.Generic;

namespace Services.Tests
{
    public static class TestsData
    {
        public static IEnumerable<object[]> BoardDtoData =>
            new List<object[]>
            {
                new object[] {1, "herearemorethantwentycharacters", "valid"},
                new object[] {1, "valid", "herearemorethanonehundredcharacters" +
                                          "herearemorethanonehundredcharacters" +
                                          "herearemorethanonehundredcharacters"},
                new object[] {1, null, "valid"},
                new object[] {1, "valid", null},
                new object[] {-1, "valid", "valid"},

                new object[] {0, "valid", "valid"}
            };

        public static IEnumerable<object[]> CardDtoData =>
            new List<object[]>
            {
                new object[] { 1, 1, "herearemorethantwentycharacters", "valid" },
                new object[] { 1, 1, "valid", "herearemorethanonehundredcharacters" +
                                              "herearemorethanonehundredcharacters" +
                                              "herearemorethanonehundredcharacters" },
                new object[] { 1, 1, null, "valid" },
                new object[] { 1, 1, "valid", null },
                new object[] { -1, 1, "valid", "valid" },
                new object[] { 1, -1, "valid", "valid" }
            };

        public static IEnumerable<object[]> ColumnDtoData =>
            new List<object[]>
            {
                new object[] { 1, 1, "herearemorethantwentycharacters" },
                new object[] { 1, 1, null },
                new object[] { -1, 1, "valid" },
                new object[] { 1, -1, "valid" }
            };

        public static IEnumerable<object[]> CommentDtoData =>
            new List<object[]>
            {
                new object[] { 1, 1, 1, "herearemorethantwohundredcharacters" +
                                        "herearemorethantwohundredcharacters" +
                                        "herearemorethantwohundredcharacters" +
                                        "herearemorethantwohundredcharacters" +
                                        "herearemorethantwohundredcharacters" +
                                        "herearemorethantwohundredcharacters" },
                new object[] { 1, 1, 1, null },
                new object[] { -1, 1, 1, "valid" },
                new object[] { 1, -1, 1, "valid" },
                new object[] { 1, 1, -1, "valid" }
            };

        public static IEnumerable<object[]> LabelDtoData =>
            new List<object[]>
            {
                new object[] { 1, "herearemorethanonehundredcharacters" +
                                  "herearemorethanonehundredcharacters" +
                                  "herearemorethanonehundredcharacters" },
                new object[] { 1, null },
                new object[] { -1, "valid" }
            };

        public static IEnumerable<object[]> PermissionDtoData =>
            new List<object[]>
            {
                new object[] { 1, "herearemorethanonehundredcharacters" +
                                  "herearemorethanonehundredcharacters" +
                                  "herearemorethanonehundredcharacters" },
                new object[] { 1, null },
                new object[] { -1, "valid" },
            };

        public static IEnumerable<object[]> UserDtoData =>
            new List<object[]>
            {
                new object[] {1, "herearemorethantwentycharacters", "valid", "valid", "valid", "valid"},
                new object[]
                {
                    1, "valid", "herearemorethanonehundredcharacters" +
                                "herearemorethanonehundredcharacters" +
                                "herearemorethanonehundredcharacters",
                    "valid", "valid", "valid"
                },
                new object[] {1, null, "valid", "valid", "valid", "valid"},
                new object[] {1, "valid", null, "valid", "valid", "valid"},
                new object[] {1, "valid", "valid", null, "valid", "valid"},
                new object[] {1, "valid", "valid", "valid", null, "valid"},
                new object[] {1, "valid", "valid", "valid", "valid", null},
                new object[] {-1, "valid", "valid", "valid", "valid", "valid"}
            };
    }
}
