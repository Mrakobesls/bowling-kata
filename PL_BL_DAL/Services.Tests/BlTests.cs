﻿using System;
using AutoMapper;
using DataBase.Model;
using Moq;
using Repository.Interfaces;
using Services.DTO;
using Services.Mapping;
using Services.Services;
using Services.Validators;
using Xunit;
using Xunit.Abstractions;

namespace Services.Tests
{
    public class BlTests
    {
        private readonly ITestOutputHelper _testOutputHelper;

        public BlTests(ITestOutputHelper testOutputHelper)
        {
            _testOutputHelper = testOutputHelper;
        }

        [Theory]
        [MemberData(nameof(TestsData.BoardDtoData), MemberType = typeof(TestsData))]
        public void ValidateBoardDto_ThrowArgumentOutOfRangeException_IfIncorrectDataEntered(int id, string title, string description)
        {
            //arrange
            var board = new Board()
            {
                Id = id,
                Title = title,
                Description = description
            };
            var boardDto = new BoardDto()
            {
                Id = board.Id,
                Title = board.Title,
                Description = board.Description
            };

            var mockRepository = new Mock<IGenericRepository<Board>>();
            mockRepository.Setup(a => a.Create(board));

            var mockMapper = new Mock<IMapper>();
            mockMapper.Setup(m => m.Map<Board>(It.IsAny<BoardDto>()))
                .Returns(board);

            var validator = new BoardValidator();

            var boardService = new BoardService(mockRepository.Object, mockMapper.Object, validator);
            //Act
            Action act = () => boardService.Create(boardDto);
            //Assert
            _testOutputHelper.WriteLine(Assert.Throws<ArgumentOutOfRangeException>(act).Message);
        }

        [Theory]
        [MemberData(nameof(TestsData.CardDtoData), MemberType = typeof(TestsData))]
        public void ValidateCardDto_ThrowArgumentOutOfRangeException_IfIncorrectDataEntered(int id, int columnId, string title, string description)
        {
            //arrange
            var card = new Card()
            {
                Id = id,
                ColumnId = columnId,
                Title = title,
                Description = description
            };
            var cardDto = new CardDto()
            {
                Id = card.Id,
                Title = card.Title,
                ColumnId = card.ColumnId,
                Description = card.Description
            };

            var mockRepository = new Mock<IGenericRepository<Card>>();
            mockRepository.Setup(a => a.Create(card));

            var mockMapper = new Mock<IMapper>();
            mockMapper.Setup(m => m.Map<Card>(It.IsAny<CardDto>()))
                .Returns(card);

            var validator = new CardValidator();

            var service = new CardService(mockRepository.Object, mockMapper.Object, validator);
            //Act
            Action act = () => service.Create(cardDto);
            //Assert
            _testOutputHelper.WriteLine(Assert.Throws<ArgumentOutOfRangeException>(act).Message);
        }

        [Theory]
        [MemberData(nameof(TestsData.ColumnDtoData), MemberType = typeof(TestsData))]
        public void ValidateColumnDto_ThrowArgumentOutOfRangeException_IfIncorrectDataEntered(int id, int boardId, string title)
        {
            //arrange
            var column = new Column()
            {
                Id = id,
                BoardId = boardId,
                Title = title
            };
            var columnDto = new ColumnDto()
            {
                Id = column.Id,
                BoardId = column.BoardId,
                Title = column.Title
            };

            var mockRepository = new Mock<IGenericRepository<Column>>();
            mockRepository.Setup(a => a.Create(column));

            var mockMapper = new Mock<IMapper>();
            mockMapper.Setup(m => m.Map<Column>(It.IsAny<ColumnDto>()))
                .Returns(column);

            var validator = new ColumnValidator();

            var service = new ColumnService(mockRepository.Object, mockMapper.Object, validator);
            //Act
            Action act = () => service.Create(columnDto);
            //Assert
            _testOutputHelper.WriteLine(Assert.Throws<ArgumentOutOfRangeException>(act).Message);
        }

        [Theory]
        [MemberData(nameof(TestsData.CommentDtoData), MemberType = typeof(TestsData))]
        public void ValidateCommentDto_ThrowArgumentOutOfRangeException_IfIncorrectDataEntered(int id, int cardId, int userId, string description)
        {
            //arrange
            var comment = new Comment()
            {
                Id = id,
                CardId = cardId,
                UserId = userId,
                Text = description,
                DateTime = DateTime.Now
            };
            var commentDto = new CommentDto()
            {
                Id = comment.Id,
                CardId = comment.CardId,
                UserId = comment.UserId,
                Text = comment.Text,
                DateTime = comment.DateTime
            };

            var mockRepository = new Mock<IGenericRepository<Comment>>();
            mockRepository.Setup(a => a.Create(comment));

            var mockMapper = new Mock<IMapper>();
            mockMapper.Setup(m => m.Map<Comment>(It.IsAny<CommentDto>()))
                .Returns(comment);

            var validator = new CommentValidator();

            var service = new CommentService(mockRepository.Object, mockMapper.Object, validator);
            //Act
            Action act = () => service.Create(commentDto);
            //Assert
            _testOutputHelper.WriteLine(Assert.Throws<ArgumentOutOfRangeException>(act).Message);
        }

        [Theory]
        [MemberData(nameof(TestsData.LabelDtoData), MemberType = typeof(TestsData))]
        public void ValidateLabelDto_ThrowArgumentOutOfRangeException_IfIncorrectDataEntered(int id, string name)
        {
            //arrange
            var label = new Label()
            {
                Id = id,
                Name = name
            };
            var labelDto = new LabelDto()
            {
                Id = label.Id,
                Name = label.Name
            };

            var mockRepository = new Mock<IGenericRepository<Label>>();
            mockRepository.Setup(a => a.Create(label));

            var mockMapper = new Mock<IMapper>();
            mockMapper.Setup(m => m.Map<Label>(It.IsAny<LabelDto>()))
                .Returns(label);

            var validator = new LabelValidator();

            var service = new LabelService(mockRepository.Object, mockMapper.Object, validator);
            //Act
            Action act = () => service.Create(labelDto);
            //Assert
            _testOutputHelper.WriteLine(Assert.Throws<ArgumentOutOfRangeException>(act).Message);
        }

        [Theory]
        [MemberData(nameof(TestsData.PermissionDtoData), MemberType = typeof(TestsData))]
        public void ValidatePermissionDto_ThrowArgumentOutOfRangeException_IfIncorrectDataEntered(int id, string name)
        {
            //arrange
            var permission = new Permission()
            {
                Id = id,
                Name = name
            };
            var permissionDto = new PermissionDto()
            {
                Id = permission.Id,
                Name = permission.Name
            };

            var mockRepository = new Mock<IGenericRepository<Permission>>();
            mockRepository.Setup(a => a.Create(permission));

            var mockMapper = new Mock<IMapper>();
            mockMapper.Setup(m => m.Map<Permission>(It.IsAny<PermissionDto>()))
                .Returns(permission);

            var validator = new PermissionValidator();

            var service = new PermissionService(mockRepository.Object, mockMapper.Object, validator);
            //Act
            Action act = () => service.Create(permissionDto);
            //Assert
            _testOutputHelper.WriteLine(Assert.Throws<ArgumentOutOfRangeException>(act).Message);
        }

        [Theory]
        [MemberData(nameof(TestsData.UserDtoData), MemberType = typeof(TestsData))]
        public void ValidateUserDto_ThrowArgumentOutOfRangeException_IfIncorrectDataEntered(int id, string email, string login, string password, string firstName, string lastName)
        {
            //arrange
            var user = new User()
            {
                Id = id,
                Email = email,
                Login = login,
                Password = password,
                FirstName = firstName,
                LastName = lastName
            };
            var userDto = new UserDto()
            {
                Id = user.Id,
                Email = user.Email,
                Login = user.Login,
                Password = user.Password,
                FirstName = user.FirstName,
                LastName = user.LastName
            };

            var mockRepo = new Mock<IGenericRepository<User>>();
            mockRepo.Setup(a => a.Create(user));

            var mockMapper = new Mock<IMapper>();
            mockMapper.Setup(m => m.Map<User>(It.IsAny<UserDto>()))
                .Returns(user);

            var validator = new UserValidator();

            var service = new UserService(mockRepo.Object, mockMapper.Object, validator);
            //Act
            Action act = () => service.Create(userDto);
            //Assert
            _testOutputHelper.WriteLine(Assert.Throws<ArgumentOutOfRangeException>(act).Message);
        }

        [Fact]
        public void ValidateMapConfiguration_ThrowNothing_IfCorrectlyConfigured()
        {
            //arrange
            var configuration = new MapperConfiguration(
                cfg => cfg.AddProfile(new MappingProfile()));
            //Act
            var record = Record.Exception(() => configuration.AssertConfigurationIsValid());
            //Assert
            Assert.Null(record);
        }
    }
}
