﻿using System.Collections.Generic;
using System.Linq;
using ConsoleApp.Model;
using DataBase;
using DataBase.Model;
using Serializer;

namespace ConsoleApp
{
    //для генерации xml 
    class Program
    {
        private static void Main()
        {
            using var context = new BaseDbContext();
            var users = new List<User>
            {
                new User()
                {
                    Id = 4,
                    Email = "reweewf@reweewf.by",
                    Login = "reweewf",
                    Password = "reweewf",
                    FirstName = "reweewf",
                    LastName = "reweewf"
                },
                new User()
                {
                    Id = 5,
                    Email = "qwer@reweewf.by",
                    Login = "qwer",
                    Password = "qwer",
                    FirstName = "qwer",
                    LastName = "qwer"
                }
            };
            XmlSerializer<List<User>>.Serialize(users, @"D:\.Internship\UserXMLMaker\SerializedUsers.xml");

            //var boards = new List<Board>
            //{
            //    new Board()
            //    {
            //        Id = 101,
            //        Title = "Fffirst",
            //        Description = "Fffirst",
            //        Columns = new List<Column>
            //        {
            //            new Column()
            //            {
            //                Id = 101,
            //                BoardId = 101,
            //                Title = "Fffirst",
            //                Cards = new List<Card>
            //                {
            //                    new Card()
            //                    {
            //                        Id = 101,
            //                        ColumnId = 101,
            //                        Title = "Fffirst",
            //                        Description = "Fffirst"
            //                    }
            //                }
            //            }

            //        }
            //    },
            //    new Board()
            //    {
            //        Id = 102,
            //        Title = "Ssecond",
            //        Description = "Ssecond",
            //        Columns = new List<Column>
            //        {
            //            new Column()
            //            {
            //                Id = 102,
            //                BoardId = 102,
            //                Title = "Ssecond",
            //                Cards = new List<Card>
            //                {
            //                    new Card()
            //                    {
            //                        Id = 102,
            //                        ColumnId = 102,
            //                        Title = "Ssecond",
            //                        Description = "Ssecond"
            //                    },
            //                    new Card()
            //                    {
            //                        Id = 103,
            //                        ColumnId = 102,
            //                        Title = "Ssecondx2",
            //                        Description = "Ssecondx2"
            //                    }
            //                }
            //            },
            //            new Column()
            //            {
            //                Id = 103,
            //                BoardId = 102,
            //                Title = "Ssecond",
            //                Cards = new List<Card>()
            //            }

            //        }
            //    }
            //};
            //XmlSerializer<List<Board>>.Serialize(boards, @"D:\.Internship\UserXMLMaker\SerializedBcc.xml");

            //List<Board> a = context.Set<Board>().ToList();
            //var boardsDes = XmlSerializer<List<Board>>.Deserialize(@"D:\.Internship\UserXMLMaker\SerializedBcc.xml");

        }
    }
}
