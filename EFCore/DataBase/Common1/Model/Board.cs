﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace ConsoleApp.Model
{
    [Serializable]
    public class Board
    {
        [XmlElement]
        public int Id { get; set; }
        [XmlElement]
        public string Title { get; set; }
        [XmlElement]
        public string Description { get; set; }
        [XmlElement]
        public List<Column> Columns { get; set; }

        public static explicit operator DataBase.Model.Board(Board board)
        {
            return new DataBase.Model.Board()
            {
                Id = board.Id,
                Title = board.Title,
                Description = board.Description,
            };
        }
    }
}
