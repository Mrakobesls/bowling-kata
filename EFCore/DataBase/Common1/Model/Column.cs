﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using ConsoleApp.Model;

namespace Common.Model
{
    [Serializable]
    public class Column
    {
        [XmlElement]
        public int Id { get; set; }
        [XmlElement]
        public int BoardId { get; set; }
        [XmlElement]
        public string Title { get; set; }
        [XmlElement]
        public List<Card> Cards { get; set; }

        public static explicit operator DataBase.Model.Column(Column column)
        {
            return new DataBase.Model.Column()
            {
                Id = column.Id,
                BoardId = column.BoardId,
                Title = column.Title
            };
        }
    }
}
