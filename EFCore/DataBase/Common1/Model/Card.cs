﻿using System;
using System.Xml.Serialization;

namespace ConsoleApp.Model
{
    [Serializable]
    public class Card
    {
        [XmlElement]
        public int Id { get; set; }
        [XmlElement]
        public int ColumnId { get; set; }
        [XmlElement]
        public string Title { get; set; }
        [XmlElement]
        public string Description { get; set; }

        public static explicit operator DataBase.Model.Card(Card card)
        {
            return new DataBase.Model.Card()
            {
                Id = card.Id,
                ColumnId = card.ColumnId,
                Title = card.Title,
                Description = card.Description,
            };
        }
    }
}
