﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataBase.EntitySeeds.Inteface
{
    internal interface ISeed<TEntity> 
        where TEntity : class
    {
        public void Seed(EntityTypeBuilder<TEntity> builder);
    }
}
