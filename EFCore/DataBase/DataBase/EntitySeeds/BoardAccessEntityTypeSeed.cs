﻿using DataBase.EntitySeeds.Inteface;
using DataBase.Model;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataBase.EntitySeeds
{
    internal class BoardAccessEntityTypeSeed : ISeed<BoardAccess>
    {
        public void Seed(EntityTypeBuilder<BoardAccess> builder)
        {
            builder.HasData(
                new BoardAccess() { UserId = 1, BoardId = 1, PermissionId = 1 },
                new BoardAccess() { UserId = 2, BoardId = 1, PermissionId = 2 },
                new BoardAccess() { UserId = 3, BoardId = 1, PermissionId = 3 }
            );
        }
    }
}
