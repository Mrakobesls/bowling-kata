﻿using DataBase.EntitySeeds.Inteface;
using DataBase.Model;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataBase.EntitySeeds
{
    internal class LabelEntityTypeSeed : ISeed<Label>
    {
        public void Seed(EntityTypeBuilder<Label> builder)
        {
            builder.HasData(
                new Label() { Id = 1, Name = "Important" },
                new Label() { Id = 2, Name = "NotReallyImportant" },
                new Label() { Id = 3, Name = "FunnyLabel" }
            );
        }
    }
}
