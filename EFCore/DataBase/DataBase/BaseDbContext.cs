﻿using Microsoft.EntityFrameworkCore;
using System.IO;
using DataBase.EntitySeeds;
using DataBase.Model;
using Microsoft.Extensions.Configuration;

namespace DataBase
{
    public class BaseDbContext : DbContext
    {
        public DbSet<Board> Boards { get; set; }
        public DbSet<BoardAccess> BoardAccesses { get; set; }
        public DbSet<Card> Cards { get; set; }
        public DbSet<Column> Columns { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<Label> Labels { get; set; }
        public DbSet<Permission> Permissions { get; set; }
        public DbSet<User> Users { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var builder = new ConfigurationBuilder();
            builder.SetBasePath(Directory.GetCurrentDirectory());
            builder.AddJsonFile(@"D:\.Internship\LocalRepo\EFCore\DataBase\DataBase\appsettings.json");
            var config = builder.Build();
            var connectionString = config.GetConnectionString("DefaultConnection");
            optionsBuilder.UseLazyLoadingProxies().UseSqlServer(connectionString);
            //optionsBuilder.UseSqlServer(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
        }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<BoardAccess>().HasKey(k => new { k.BoardId, k.UserId });
            modelBuilder.Entity<User>().HasAlternateKey(ak => ak.Login);
            modelBuilder.Entity<User>().HasAlternateKey(ak => ak.Email);

            new BoardAccessEntityTypeSeed().Seed(modelBuilder.Entity<BoardAccess>());
            new BoardEntityTypeSeed().Seed(modelBuilder.Entity<Board>());
            new CardEntityTypeSeed().Seed(modelBuilder.Entity<Card>());
            new ColumnEntityTypeSeed().Seed(modelBuilder.Entity<Column>());
            new CommentEntityTypeSeed().Seed(modelBuilder.Entity<Comment>());
            new LabelEntityTypeSeed().Seed(modelBuilder.Entity<Label>());
            new PermissionEntityTypeSeed().Seed(modelBuilder.Entity<Permission>());
            new UserEntityTypeSeed().Seed(modelBuilder.Entity<User>());
        }
    }
}
