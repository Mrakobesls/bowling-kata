﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DataBase.Model
{
    public class Card
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required]
        public int ColumnId { get; set; }
        [ForeignKey("ColumnId")]
        public virtual Column Column { get; set; }

        [Required]
        [MaxLength(20)]
        public string Title { get; set; }
        [Required]
        [MaxLength(100)]
        public string Description { get; set; }
        public virtual ICollection<Label> Labels { get; set; } = new HashSet<Label>();
        public virtual ICollection<Comment> Comments { get; set; } = new HashSet<Comment>();
        public virtual ICollection<User> Users { get; set; } = new HashSet<User>();
    }
}
