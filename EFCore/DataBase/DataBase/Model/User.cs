﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Xml.Serialization;
using Microsoft.EntityFrameworkCore;

namespace DataBase.Model
{
    [Serializable]
    [Index(nameof(FirstName), nameof(LastName))]
    public class User
    {
        [XmlIgnore]
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [XmlElement]
        [Required]
        [MaxLength(30)]
        public string Email { get; set; }

        [XmlElement]
        [Required]
        [MaxLength(20)]
        public string Login { get; set; }

        [XmlElement]
        [Required]
        [MaxLength(20)]
        public string Password { get; set; }

        [XmlElement]
        [Required]
        [MaxLength(25)]
        public string FirstName { get; set; }

        [XmlElement]
        [Required]
        [MaxLength(25)]
        public string LastName { get; set; }

        [XmlIgnore]
        public virtual ICollection<BoardAccess> BoardAccesses { get; set; } = new HashSet<BoardAccess>();
        [XmlIgnore]
        public virtual ICollection<Card> Cards { get; set; } = new HashSet<Card>();
        [XmlIgnore]
        public virtual ICollection<Comment> Comments { get; set; } = new HashSet<Comment>();
    }
}
