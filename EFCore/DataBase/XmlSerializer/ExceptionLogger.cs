﻿using System;
using System.IO;

namespace Serializer
{
    public class ExceptionLogger
    {
        public static void LogInfo(Exception exc, string filePath)
        {
            using var writer = new StreamWriter(Path.GetDirectoryName(filePath) + "\\Exceptionlogger.txt", true);
            writer.WriteLine("{0:dd/MM/yyyy hh:mm:ss}: file {1} issued an error {2}", DateTime.Now, filePath, exc.Message);
            writer.Flush();
        }
    }
}
