﻿using System;
using System.IO;
using System.Xml.Serialization;

namespace Serializer
{
    public static class XmlSerializer<T>
    {
        public static void Serialize(T instance, string filePath)
        {
            var formatter = new XmlSerializer(typeof(T));
            using var fs = new FileStream(filePath, FileMode.OpenOrCreate);
            formatter.Serialize(fs, instance);
        }
        public static T Deserialize(string filePath)
        {
            try
            {
                var formatter = new XmlSerializer(typeof(T));
                using var fs = new FileStream(filePath, FileMode.OpenOrCreate);
                return (T)formatter.Deserialize(fs);
            }
            catch (Exception e)
            {
                ExceptionLogger.LogInfo(e, filePath);
                return default;
            }
        }
    }
}
