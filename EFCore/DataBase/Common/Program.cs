﻿using System.Collections.Generic;
using DataBase;
using DataBase.Model;
using Serializer;

namespace ConsoleApp
{
    //для генерации xml
    class Program
    {
        private static void Main()
        {
            //using var context = new BaseDbContext();
            ////var users = new List<User>
            ////{
            ////    new User()
            ////    {
            ////        Email = "reweewf@reweewf.by",
            ////        Login = "reweewf",
            ////        Password = "reweewf",
            ////        FirstName = "reweewf",
            ////        LastName = "reweewf"
            ////    },
            ////    new User()
            ////    {
            ////        Email = "qwer@reweewf.by",
            ////        Login = "qwer",
            ////        Password = "qwer",
            ////        FirstName = "qwer",
            ////        LastName = "qwer"
            ////    }
            ////};
            ////XmlSerializer<List<User>>.Serialize(users, @"D:\.Internship\UserXMLMaker\SerializedUsers.xml");

            var boards = new List<Common.Model.Board>
            {
                new Common.Model.Board()
                {
                    Title = "Fffirst",
                    Description = "Fffirst",
                    Columns = new List<Common.Model.Column>
                    {
                        new Common.Model.Column()
                        {
                            Title = "Fffirst",
                            Cards = new List<Common.Model.Card>
                            {
                                new Common.Model.Card()
                                {
                                    Title = "Fffirst",
                                    Description = "Fffirst"
                                }
                            }
                        }

                    }
                },
                new Common.Model.Board()
                {
                    Title = "Ssecond",
                    Description = "Ssecond",
                    Columns = new List<Common.Model.Column>
                    {
                        new Common.Model.Column()
                        {
                            Title = "Ssecond",
                            Cards = new List<Common.Model.Card>
                            {
                                new Common.Model.Card()
                                {
                                    Title = "Ssecond",
                                    Description = "Ssecond"
                                },
                                new Common.Model.Card()
                                {
                                    Title = "Ssecondx2",
                                    Description = "Ssecondx2"
                                }
                            }
                        },
                        new Common.Model.Column()
                        {
                            Title = "Ssecond",
                            Cards = new List<Common.Model.Card>()
                        }

                    }
                }
            };
            XmlSerializer<List<Common.Model.Board>>.Serialize(boards, @"D:\.Internship\UserXMLMaker\SerializedBcc.xml");


            //List<Board> a = context.Set<Board>().ToList();
            //var boardsDes = XmlSerializer<List<Board>>.Deserialize(@"D:\.Internship\UserXMLMaker\SerializedBcc.xml");

        }
    }
}
