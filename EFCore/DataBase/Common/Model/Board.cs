﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Common.Model
{
    [Serializable]
    public class Board
    {
        [XmlElement]
        public string Title { get; set; }
        [XmlElement]
        public string Description { get; set; }
        [XmlElement]
        public List<Column> Columns { get; set; }

        public static implicit operator DataBase.Model.Board(Board board)
        {
            return new DataBase.Model.Board()
            {
                Title = board.Title,
                Description = board.Description,
            };
        }
    }
}
