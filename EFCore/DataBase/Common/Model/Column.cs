﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Common.Model
{
    [Serializable]
    public class Column
    {
        [XmlElement]
        public string Title { get; set; }
        [XmlElement]
        public List<Card> Cards { get; set; }

        public static explicit operator DataBase.Model.Column(Column column)
        {
            return new DataBase.Model.Column()
            {
                Title = column.Title
            };
        }
    }
}
