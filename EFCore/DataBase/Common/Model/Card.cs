﻿using System;
using System.Xml.Serialization;

namespace Common.Model
{
    [Serializable]
    public class Card
    {
        [XmlElement]
        public string Title { get; set; }
        [XmlElement]
        public string Description { get; set; }

        public static explicit operator DataBase.Model.Card(Card card)
        {
            return new DataBase.Model.Card()
            {
                Title = card.Title,
                Description = card.Description,
            };
        }
    }
}
