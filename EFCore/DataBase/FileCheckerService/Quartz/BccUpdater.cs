﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Quartz;
using System.Threading.Tasks;
using DataBase;
using DataBase.Model;
using Microsoft.EntityFrameworkCore;
using Serializer;

namespace FileCheckerService.Quartz
{
    [DisallowConcurrentExecution]
    internal class BccUpdater : IJob
    {
        public async Task Execute(IJobExecutionContext context)
        {
            var files = GetFiles();
            if (files.Count == 0) return;

            await using var dbContext = new BaseDbContext();

            var xmlData = new List<XmlDataWay>();
            foreach (var filePath in files)
            {
                xmlData.Add(new XmlDataWay(XmlSerializer<List<Common.Model.Board>>.Deserialize(filePath), filePath));
                File.Delete(filePath);
            }
            

            var userId = dbContext.Users.FirstOrDefaultAsync().Result.Id;
            foreach (var xmlDataWay in xmlData)
            {
                if (xmlDataWay.Boards is null)
                    continue;
                if (xmlDataWay.Boards.Count == 0) continue;

                foreach (var board in xmlDataWay.Boards)
                {
                    var dbBoard = (Board) board;
                    dbContext.Attach(dbBoard);

                    dbBoard.BoardAccesses.Add(new BoardAccess()
                    {
                        UserId = userId,
                        PermissionId = 1
                    });
                    if (board.Columns.Count == 0) continue;
                    foreach (var column in board.Columns)
                    {
                        var dbColumn = (Column) column;
                        dbBoard.Columns.Add(dbColumn);

                        if (column.Cards.Count == 0) continue;
                        foreach (var card in column.Cards)
                        {
                            var dbCard = (Card) card;
                            dbColumn.Cards.Add(dbCard);
                        }
                    }
                }
            }
            await dbContext.SaveChangesAsync();
            Console.WriteLine("Yhaha");
        }

        public List<string> GetFiles()
        {
            return Directory.GetFiles("D:\\.Internship\\LocalRepo\\EFCore\\DataBase\\Boards_File", "*.xml").ToList();
        }

        private class XmlDataWay
        {
            public XmlDataWay(List<Common.Model.Board> boards, string fileWay)
            {
                Boards = boards;
                FileWay = fileWay;
            }
            public readonly List<Common.Model.Board> Boards;
            public readonly string FileWay;
        }
    }
}
