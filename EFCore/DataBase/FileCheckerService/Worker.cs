using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;
using FileCheckerService.Quartz;

namespace FileCheckerService
{
    public class Worker : BackgroundService
    {
        private readonly UserUpdater _userUpdater;

        public Worker(UserUpdater userUpdater)
        {
            _userUpdater = userUpdater;
        }

        public override Task StartAsync(CancellationToken CancellationToken)
        {
            _userUpdater.Start();
            return base.StartAsync(CancellationToken);
        }


        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            return Task.CompletedTask;
        }

        public override Task StopAsync(CancellationToken cancellationToken)
        {
            _userUpdater.Stop();
            return base.StopAsync(cancellationToken);
        }
    }
}
