﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using DataBase;
using DataBase.Model;
using Serializer;

namespace FileCheckerService
{
    public class UserUpdater : IDisposable
    {
        private readonly FileSystemWatcher _watcher;
        public UserUpdater()
        {
            _watcher = new FileSystemWatcher(@"D:\.Internship\LocalRepo\EFCore\DataBase\Users_File");
            _watcher.Created += Watcher_Created;
        }

        public void Start()
        {
            _watcher.EnableRaisingEvents = true;
        }
        public void Stop()
        {
            _watcher.EnableRaisingEvents = false;
        }
        private void Watcher_Created(object sender, FileSystemEventArgs e)
        {
            if (Path.GetExtension(e.FullPath) != ".xml")
                return;
            var users = XmlSerializer<List<User>>.Deserialize(e.FullPath);
            File.Delete(e.FullPath);

            if (users is null)
                return;
            if (users.Count == 0) return;

            using var dbContext = new BaseDbContext();
            foreach (var item in users)
            {
                try
                {
                    var dbUser = dbContext.Users.FirstOrDefault(u => u.Login == item.Login);
                    if (dbUser != null)
                        throw new Exception($"A user with the login {item.Login} already exists in the database with id {dbUser.Id}.");
                    dbContext.Add(item);
                }
                catch (Exception exception)
                {
                    ExceptionLogger.LogInfo(exception, e.FullPath);
                }

            }

            dbContext.SaveChanges();
        }

        public void Dispose()
        {
            Stop();
            _watcher?.Dispose();
        }
    }
}
