﻿using System;
using System.Collections.Generic;
using Observer.Clients;
using Observer.Products;
using Observer.Shops;

namespace Observer
{
    internal class Program
    {
        private static void Main()
        {
            var clients = new List<Client>
                { new Client(), new Client(), new Client() };
            var shop = new Shop();
            shop.AddProduct(new Sofa(5));
            shop.AddProduct(new Sofa(7));
            shop.DeleteProduct(new Sofa(3));
            shop.AddProduct(new Chair(5));

            shop.Attach(clients[0], typeof(Sofa));
            shop.AddProduct(new Sofa(12));
            shop.Attach(clients[1], typeof(Sofa));
            shop.Attach(clients[2], typeof(Sofa));
            shop.AddProduct(new Sofa(5));

            shop.Attach(clients[2], typeof(Table));
            shop.AddProduct(new Chair(4));
            shop.AddProduct(new Table(8));
            shop.DeleteProduct(new Table(3));
        }
    }
}
