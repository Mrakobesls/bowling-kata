﻿using System.Collections.Generic;
using Observer.Clients.Messages;

namespace Observer.Clients
{
    internal class Client : IClient, IObserver
    {
        private readonly List<IMessage> _mail = new List<IMessage>();

        public void Update(IMessage message)
        {
            _mail.Add(message);
        }
    }
}
