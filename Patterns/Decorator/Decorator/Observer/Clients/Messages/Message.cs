﻿using System;

namespace Observer.Clients.Messages
{
    internal class Message : IMessage
    {
        public DateTime Date { get; set; }
        public string Topic { get; set; }
        public string Text { get; set; }

        public Message(string topic, string text)
        {
            Date = DateTime.Now;
            Topic = topic;
            Text = text;
        }
    }
}
