﻿using System;

namespace Observer.Clients.Messages
{
    internal interface IMessage
    {
        public DateTime Date { get; set; }
        public string Topic { get; set; }
        public string Text { get; set; }
    }
}
