﻿using Observer.Clients.Messages;

namespace Observer.Clients
{
    internal interface IObserver
    {
        void Update(IMessage message);
    }
}
