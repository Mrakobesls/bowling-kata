﻿using Observer.Products.Interface;

namespace Observer.Products
{
    internal class Chair : IProduct
    {
        public int Count { get; set; }

        public Chair(int count = 0)
        {
            Count = count;
        }
    }
}
