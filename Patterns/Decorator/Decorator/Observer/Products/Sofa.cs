﻿using System;
using System.Collections.Generic;
using System.Text;
using Observer.Products.Interface;

namespace Observer.Products
{
    internal class Sofa : IProduct
    {
        public int Count { get; set; }

        public Sofa(int count = 0)
        {
            Count = count;
        }
    }
}
