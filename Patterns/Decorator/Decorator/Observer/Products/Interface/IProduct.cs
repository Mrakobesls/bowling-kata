﻿namespace Observer.Products.Interface
{
    internal interface IProduct
    {
        public int Count { get; set; }

    }
}
