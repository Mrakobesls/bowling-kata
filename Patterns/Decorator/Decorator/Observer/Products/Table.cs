﻿using Observer.Products.Interface;

namespace Observer.Products
{
    internal class Table : IProduct
    {
        public int Count { get; set; }

        public Table(int count = 0)
        {
            Count = count;
        }
    }
}
