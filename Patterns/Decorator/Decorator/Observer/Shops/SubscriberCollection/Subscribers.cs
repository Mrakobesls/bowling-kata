﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Observer.Clients;
using Observer.Shops.Publisher;

namespace Observer.Shops.SubscriberCollection
{
    internal class Subscribers : ISubscribers, IManageObservers, IEnumerable<ProductSubs>
    {
        private readonly List<ProductSubs> _subs = new List<ProductSubs>();

        public ProductSubs this[Type product] => FindProductSubs(product);

        public void Attach(IObserver observer, Type product)
        {
            if (FindProductSubs(product) is null)
            {
                _subs.Add(new ProductSubs(product));
            }
            this[product].Add(observer);
        }

        public void Detach(IObserver observer, Type product)
        {
            if (FindProductSubs(product) is null)
            {
                _subs.Add(new ProductSubs(product));
            }
            this[product].Remove(observer);
        }

        public ProductSubs FindProductSubs(Type product)
        {
            return _subs.FirstOrDefault(item => product.Name == item.Prod.Name);
        }
        public IEnumerator<ProductSubs> GetEnumerator()
        {
            return _subs.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        //Если реализовывать интерфейс IList<T> появляется функционал который никогда в теории не будет использован,
        //в отличие от ShopProducts, где этот интерфейс отлично дополнил класс

        //public void Add(ProductSubs item)
        //{
        //    throw new NotImplementedException();
        //}

        //public void Clear()
        //{
        //    throw new NotImplementedException();
        //}

        //public bool Contains(ProductSubs item)
        //{
        //    throw new NotImplementedException();
        //}

        //public void CopyTo(ProductSubs[] array, int arrayIndex)
        //{
        //    throw new NotImplementedException();
        //}

        //public bool Remove(ProductSubs item)
        //{
        //    throw new NotImplementedException();
        //}

        //public int Count { get; }
        //public bool IsReadOnly { get; }
        //public int IndexOf(ProductSubs item)
        //{
        //    throw new NotImplementedException();
        //}

        //public void Insert(int index, ProductSubs item)
        //{
        //    throw new NotImplementedException();
        //}

        //public void RemoveAt(int index)
        //{
        //    throw new NotImplementedException();
        //}

        //public ProductSubs this[int index]
        //{
        //    get => throw new NotImplementedException();
        //    set => throw new NotImplementedException();
        //}
    }
}
