﻿using System;
using System.Collections;
using System.Collections.Generic;
using Observer.Clients;
using Observer.Products;
using Observer.Products.Interface;
using Observer.Shops.Publisher;

namespace Observer.Shops.SubscriberCollection
{
    internal class ProductSubs : IEnumerable<IObserver>
    {
        private readonly List<IObserver> _productSubs = new List<IObserver>();
        public Type Prod;

        public ProductSubs(Type product)
        {
            Prod = product;
        }

        public IEnumerator<IObserver> GetEnumerator()
        {
            return _productSubs.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Add(IObserver observer)
        {
            _productSubs.Add(observer);
        }

        public void Remove(IObserver observer)
        {
            _productSubs.Remove(observer);
        }
    }
}
