﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Observer.Products.Interface;

namespace Observer.Shops.ProductCollection
{
    internal class ShopProducts : IList<IProduct>
    {
        private readonly List<IProduct> _products = new List<IProduct>();
        public int Count => _products.Count;
        public bool IsReadOnly { get; }

        public ShopProducts()
        {
            IsReadOnly = false;
        }

        public IProduct this[IProduct product]
        {
            get
            {
                return _products.FirstOrDefault(item => product.GetType().Name == item.GetType().Name);
            }
        }

        public int GetCountOfProduct(Type product)
        {
            return _products.FirstOrDefault(item => product.Name == item.GetType().Name)?.Count ?? 0;
        }

        public IEnumerator<IProduct> GetEnumerator()
        {
            return _products.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Add(IProduct product)
        {
            if (_products.Exists(item => product.GetType().Name == item.GetType().Name))
                this[product].Count += product.Count;
            else
                _products.Add(product);
        }

        public void Clear()
        {
            _products.Clear();
        }

        public bool Contains(IProduct product)
        {
            return _products.Contains(product);
        }

        public void CopyTo(IProduct[] array, int arrayIndex)
        {
            _products.CopyTo(array, arrayIndex);
        }

        public bool Remove(IProduct product)
        {
            if (_products.Exists(item => product != null && product.GetType().Name == item.GetType().Name))
            {
                this[product].Count -= product.Count;
                return true;
            }
            return false;

        }

        public int IndexOf(IProduct product)
        {
            return _products.IndexOf(product);
        }

        public void Insert(int index, IProduct product)
        {
            _products.Insert(index, product);
        }

        public void RemoveAt(int index)
        {
            _products.RemoveAt(index);
        }

        public IProduct this[int index]
        {
            get => _products[index];
            set => _products[index] = value;
        }
    }
}
