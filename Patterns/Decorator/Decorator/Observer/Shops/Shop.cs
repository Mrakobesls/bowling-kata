﻿using System;
using System.Collections.Generic;
using System.Linq;
using Observer.Clients;
using Observer.Clients.Messages;
using Observer.Products;
using Observer.Products.Interface;
using Observer.Shops.Publisher;
using Observer.Shops.SubscriberCollection;
using Observer.Shops.ProductCollection;

namespace Observer.Shops
{
    internal class Shop : IShop, IPublisher
    {
        private readonly Subscribers _subscribers = new Subscribers();
        private readonly ShopProducts _products = new ShopProducts();
        public void AddProduct(IProduct product)
        {
            _products.Add(product);
            Notify(product.GetType());
        }
        public void DeleteProduct(IProduct product)
        {
            _products.Remove(product);
            Notify(product.GetType());
        }

        public void Attach(IObserver observer, Type product)
        {
            _subscribers.Attach(observer, product);
            var message = new Message($"You have subscribed to {product.Name}", $"Current number of {product.Name} is {_products.GetCountOfProduct(product)}");
            observer.Update(message);
        }

        public void Detach(IObserver observer, Type product)
        {
            _subscribers.Detach(observer, product);
        }
        public void Notify(Type product)
        {
            var message = new Message($"Updated count of {product.Name}", $"Current number of {product.Name} is {_products.GetCountOfProduct(product)}");
            if (_subscribers[product] is { })
                foreach (var item in _subscribers[product])
                    item.Update(message);

        }
    }
}
