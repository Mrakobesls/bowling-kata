﻿using System;

namespace Observer.Shops.Publisher
{
    internal interface IPublisher : IManageObservers
    {
        void Notify(Type product);
    }
}
