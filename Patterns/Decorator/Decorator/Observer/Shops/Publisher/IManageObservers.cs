﻿using System;
using Observer.Clients;
using Observer.Products;
using Observer.Products.Interface;

namespace Observer.Shops.Publisher
{
    internal interface IManageObservers
    {
        void Attach(IObserver observer, Type product);
        void Detach(IObserver observer, Type product);
    }
}
