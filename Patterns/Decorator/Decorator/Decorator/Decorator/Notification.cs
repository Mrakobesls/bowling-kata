﻿using System;
using System.Collections.Generic;
using System.Text;
using Decorator.Decorator.Intefaces;

namespace Decorator.Decorator
{
    internal class Notification : INotification
    {
        public virtual void Notify(string message)
        {
            Console.WriteLine("mail: " + message);
        }
    }
}
