﻿using System;
using System.Collections.Generic;
using System.Text;
using Decorator.Decorator.Intefaces;

namespace Decorator.Decorator
{
    internal class DecoratorSlack : Decorator
    {
        public DecoratorSlack(INotification notification) : base(notification)
        {
        }

        public override void Notify(string message)
        {
            base.Notify(message);
            Console.WriteLine("Slack: " + message);
        }
    }
}
