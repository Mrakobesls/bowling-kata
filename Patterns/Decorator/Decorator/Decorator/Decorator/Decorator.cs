﻿using System;
using System.ComponentModel;
using Decorator.Decorator.Intefaces;

namespace Decorator.Decorator
{
    internal abstract class Decorator : INotification
    {
        protected INotification Notification;

        protected Decorator(INotification notification)
        {
            Notification = notification;
        }

        public virtual void Notify(string message)
        {
            Notification?.Notify(message);
        }
    }
}
