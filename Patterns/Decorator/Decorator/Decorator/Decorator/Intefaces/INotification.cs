﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Decorator.Decorator.Intefaces
{
    internal interface INotification
    {
        void Notify(string message);
    }
}
