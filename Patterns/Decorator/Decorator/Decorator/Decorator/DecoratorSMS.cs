﻿using System;
using System.Collections.Generic;
using System.Text;
using Decorator.Decorator.Intefaces;

namespace Decorator.Decorator
{
    internal class DecoratorSMS : Decorator
    {
        public DecoratorSMS(INotification notification) : base(notification)
        {
        }

        public override void Notify(string message)
        {
            base.Notify(message);
            Console.WriteLine("SMS: " + message);
        }
    }
}
