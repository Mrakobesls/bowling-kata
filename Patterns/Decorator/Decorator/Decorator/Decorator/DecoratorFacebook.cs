﻿using System;
using System.Collections.Generic;
using System.Text;
using Decorator.Decorator.Intefaces;

namespace Decorator.Decorator
{
    internal class DecoratorFacebook : Decorator
    {
        public DecoratorFacebook(INotification notification) : base(notification)
        {
        }

        public override void Notify(string message)
        {
            base.Notify(message);
            Console.WriteLine("Facebook: " + message);
        }
    }
}
