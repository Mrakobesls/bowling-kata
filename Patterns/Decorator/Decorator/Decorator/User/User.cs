﻿using Decorator.Decorator;
using Decorator.Decorator.Intefaces;
using Decorator.User.Intefaces;

namespace Decorator.User
{
    internal class User : IUser
    {
        private INotification Notifier { get; set; }
        public bool Facebook { get; set; }
        public bool SMS { get; set; }
        public bool Slack { get; set; }
        public User()
        {
            Notifier = new Notification();
        }

        public void Notify(string message)
        {
            AssembleNotifier();
            Notifier.Notify(message);
        }
        private void AssembleNotifier()
        {
            Notifier = new Notification();
            if (Facebook)
                Notifier = new DecoratorFacebook(Notifier);
            if (Slack)
                Notifier = new DecoratorSlack(Notifier);
            if (SMS)
                Notifier = new DecoratorSMS(Notifier);
        }
    }
}
