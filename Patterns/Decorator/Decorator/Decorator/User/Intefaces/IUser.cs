﻿using Decorator.Decorator.Intefaces;

namespace Decorator.User.Intefaces
{
    internal interface IUser
    {
        bool Slack { get; set; }
        bool Facebook { get; set; }
        bool SMS { get; set; }
        void Notify(string message);
    }
}
