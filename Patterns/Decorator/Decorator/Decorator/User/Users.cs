﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using Decorator.Decorator;

namespace Decorator.User
{
    internal class Users : IEnumerable<User>
    {
        private readonly List<User> _usersCollection;

        public Users()
        {
            _usersCollection = new List<User>();
            for (var i = 0; i < 4; i++)
            {
                _usersCollection.Add(new User());

                _usersCollection[i].Facebook = Convert.ToInt32(ConfigurationManager.AppSettings[$"{i + 1}_Facebook"]) != 0;
                _usersCollection[i].Slack = Convert.ToInt32(ConfigurationManager.AppSettings[$"{i + 1}_Slack"]) != 0;
                _usersCollection[i].SMS = Convert.ToInt32(ConfigurationManager.AppSettings[$"{i + 1}_SMS"]) != 0;
            }
        }

        public IEnumerator<User> GetEnumerator()
        {
            return _usersCollection.GetEnumerator();
        }
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
