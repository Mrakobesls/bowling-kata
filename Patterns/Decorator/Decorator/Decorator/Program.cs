﻿using System;
using Decorator.User;
using Decorator.User.Intefaces;

namespace Decorator
{
    internal class Program
    {
        private static void Main()
        {
            var i = 1;
            foreach (var item in new Users())
            {
                Console.WriteLine($"{i} ");
                item.Notify("HII");
                Console.WriteLine();
                ++i;
            }
            //
            var whilik = true;
            while (whilik)
            {
                var user = new User.User();
                InitializeUser(user);
                Console.WriteLine("\nEnter a message");
                user.Notify(Console.ReadLine());
                switch (Console.ReadKey().Key)
                {
                    case ConsoleKey.Escape:
                        whilik = false;
                        break;
                    default:
                        Console.Clear();
                        break;
                }
            }
        }

        private static void InitializeUser(IUser user)
        {
            Console.WriteLine("1-true, 0 - false");
            Console.Write("Slack: ");
            user.Slack = WhiteBool();
            Console.WriteLine($"\b{user.Slack}");

            Console.Write("Facebook: ");
            user.Facebook = WhiteBool();
            Console.WriteLine($"\b{user.Facebook}");

            Console.Write("SMS: ");
            user.SMS = WhiteBool();
            Console.WriteLine($"\b{user.SMS}");
        }

        private static bool WhiteBool()
        {
            int answer;
            Int32.TryParse(Console.ReadKey().KeyChar.ToString(), out answer);
            return answer == 1;
        }

    }
}
