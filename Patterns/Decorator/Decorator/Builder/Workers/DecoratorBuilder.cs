﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Builder.Workers
{
    internal class DecoratorBuilder : AbstractBuilder
    {
        public DecoratorBuilder()
        {
            BuildFoundationPerfomance = 20;
            BuildWallsPerfomance = 35;
            BuildDoorsPerfomance = 30;
            BuildWindowsPerfomance = 30;
            BuildRoofPerfomance = 20;
            PaintHousePerfomance = 95;
            MakeFloorPerfomance = 80;
            PasteWallpaperPerfomance = 90;
        }
    }
}
