﻿using System;

namespace Builder.Workers
{
    internal class RandomBuilder : AbstractBuilder
    {
        public RandomBuilder()
        {
            var rand = new Random();
            BuildFoundationPerfomance = rand.Next() % 100 + 1;
            BuildWallsPerfomance = rand.Next() % 100 + 1;
            BuildDoorsPerfomance = rand.Next() % 100 + 1;
            BuildWindowsPerfomance = rand.Next() % 100 + 1;
            BuildRoofPerfomance = rand.Next() % 100 + 1;
            PaintHousePerfomance = rand.Next() % 100 + 1;
            MakeFloorPerfomance = rand.Next() % 100 + 1;
            PasteWallpaperPerfomance = rand.Next() % 100 + 1;
        }
    }
}
