﻿using System;
using System.Collections.Generic;
using Builder.HouseComponents;

namespace Builder.Workers
{
    internal abstract class AbstractBuilder : IBuilder
    {
        public int BuildFoundationPerfomance;
        public int BuildWallsPerfomance;
        public int BuildDoorsPerfomance;
        public int BuildWindowsPerfomance;
        public int BuildRoofPerfomance;
        public int PaintHousePerfomance;
        public int MakeFloorPerfomance;
        public int PasteWallpaperPerfomance;

        protected House House { get; set; } = new House();

        public void Reset()
        {
            House = new House();
        }

        public void SetHouse(House house)
        {
            House = house;
        }

        public House BuildFoundation()
        {
            House.Foundation = new Foundation();
            return House;
        }

        public House BuildWalls(int count)
        {
            House.Walls = new List<Wall>();
            for (var i = 0; i < count; i++)
                House.Walls.Add(new Wall());
            return House;
        }

        public House BuildDoors(int count)
        {
            House.Doors = new List<Door>();
            for (var i = 0; i < count; i++)
                House.Doors.Add(new Door());
            return House;
        }

        public House BuildWindows(int count)
        {
            House.Windows = new List<Window>();
            for (var i = 0; i < count; i++)
                House.Windows.Add(new Window());
            return House;
        }

        public House BuildRoof()
        {
            House.Roof = new Roof();
            return House;
        }

        public House PaintHouse()
        {
            House.Paint = new Paint();
            return House;
        }

        public House MakeFloor()
        {
            House.Floor = new Floor();
            return House;
        }

        public House PasteWallpaper()
        {
            House.Wallpaper = new Wallpaper();
            return House;
        }

        public int GetPerfomance(States state)
        {
            return state switch
            {
                States.Foundation => BuildFoundationPerfomance,
                States.Walls => BuildWallsPerfomance,
                States.Doors => BuildDoorsPerfomance,
                States.Windows => BuildWindowsPerfomance,
                States.Paint => PaintHousePerfomance, 
                States.Roof => BuildRoofPerfomance,
                States.Floor => MakeFloorPerfomance,
                States.Wallpaper => PasteWallpaperPerfomance,
                _ => throw new ArgumentOutOfRangeException(nameof(state), state, null)
            };
        }
    }
}
