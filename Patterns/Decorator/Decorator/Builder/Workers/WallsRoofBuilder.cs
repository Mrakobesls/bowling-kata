﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Builder.Workers
{
    internal class WallsRoofBuilder : AbstractBuilder
    {
        public WallsRoofBuilder()
        {
            BuildFoundationPerfomance = 40;
            BuildWallsPerfomance = 95;
            BuildDoorsPerfomance = 40; 
            BuildWindowsPerfomance = 40;
            BuildRoofPerfomance = 90;
            PaintHousePerfomance = 25;
            MakeFloorPerfomance = 10;
            PasteWallpaperPerfomance = 25;
        }
    }
}
