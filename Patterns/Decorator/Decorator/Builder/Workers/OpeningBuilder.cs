﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Builder.Workers
{
    internal class OpeningBuilder : AbstractBuilder
    {
        public OpeningBuilder()
        {
            BuildFoundationPerfomance = 20;
            BuildWallsPerfomance = 25;
            BuildDoorsPerfomance = 90;
            BuildWindowsPerfomance = 95;
            BuildRoofPerfomance = 20;
            PaintHousePerfomance = 35;
            MakeFloorPerfomance = 40;
            PasteWallpaperPerfomance = 45;
        }
    }
}
