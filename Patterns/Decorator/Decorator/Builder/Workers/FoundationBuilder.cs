﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Builder.Workers
{
    internal class FoundationBuilder : AbstractBuilder
    {
        public FoundationBuilder()
        {
            BuildFoundationPerfomance = 90;
            BuildWallsPerfomance = 50;
            BuildDoorsPerfomance = 30;
            BuildWindowsPerfomance = 30;
            BuildRoofPerfomance = 40;
            PaintHousePerfomance = 10;
            MakeFloorPerfomance = 25;
            PasteWallpaperPerfomance = 5;
        }
    }
}
