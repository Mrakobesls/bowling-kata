﻿using Builder.HouseComponents;

namespace Builder.Workers
{
    internal interface IBuilder
    {
        public int GetPerfomance(States state);
        public void Reset();
        public void SetHouse(House house);
        public House BuildFoundation();
        public House BuildWalls(int count);
        public House BuildDoors(int count);
        public House BuildWindows(int count);
        public House BuildRoof();
        public House PaintHouse();
        public House MakeFloor();
        public House PasteWallpaper();

    }
}
