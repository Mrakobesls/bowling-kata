﻿using System;
using System.Collections.Generic;
using Builder.Workers;

namespace Builder
{
    internal class Program
    {
        //если во время сборки переопределяешь ограничивающий элеменет(по типу стен)
        //ломается всё что от него зависит (если построил стены и крышу и переопределил стены то крышу снесёт:)
        private static void Main()
        {
            var builders = new List<IBuilder>();
            builders.Add(new FoundationBuilder());
            builders.Add(new WallsRoofBuilder());
            builders.Add(new OpeningBuilder());
            builders.Add(new DecoratorBuilder());
            var director = new Director(builders);
            //var director = new Director(new List<IBuilder>()); // - в директора обязательно передаются строители

            director.BuildFoundation();

            director.BuildWalls(4);
            director.BuildDoors(1);
            director.BuildRoof();
            director.BuildWindows(3);

            director.BuildWalls(6); // снёс двери, окна, крышу, но переопределил стены и оставил фундамент
            //director.PasteWallpaper(); // - исключение
            director.BuildRoof();
            director.PasteWallpaper();
            director.BuildDoors(2);
            //director.BuildWindows(6);
            director.MakeFloor();
            director.PaintHouse();
            Console.WriteLine(director.CurrentHouse); // если дом не готов получишь эксепшен
        }
    }
}
