﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Builder
{
    internal class BuildState
    {
        private readonly List<PointState> _condition = new List<PointState>();

        public BuildState()
        {
            foreach (var item in Enum.GetValues(typeof(States)).OfType<States>().ToList())
            {
                _condition.Add(item);
            }

            foreach (var item in _condition.Where(ps => ps.State == States.Foundation || ps.State == States.Walls || ps.State == States.Roof))
            {
                item.IsLimitting = true;
            }
        }

        public bool IsBuildReady()
        {
            return this[States.Foundation] && this[States.Walls] && this[States.Doors] && this[States.Windows] && this[States.Roof];
        }

        public bool IsPossibleToBuild(States state)
        {
            foreach (var item in _condition)
            {
                if (item.State == state) return true;
                if (!item.IsReady && item.IsLimitting)
                    return false;
            }
            return false;
        }

        public void MarkAsDone(States state)
        {
            var st = _condition.First(ps => ps.State == state);
            if (!st.IsLimitting || !st.IsReady)
            {
                st.IsReady = true;
                return;
            }

            var isPasses = false;
            foreach (var item in _condition)
            {
                if (isPasses)
                    item.IsReady = false;
                if (item.State == state)
                    isPasses = true;
            }
        }
        public bool this[States state] => _condition.First(ps => ps.State == state).IsReady;

        public void Reset()
        {
            foreach (var item in _condition)
                item.IsReady = false;
        }

        internal class PointState
        {
            public States State;
            public bool IsReady;
            public bool IsLimitting;

            public PointState(States state)
            {
                State = state;
            }
            public static implicit operator PointState(States state)
            {
                return new PointState(state);
            }
        }
    }
}
