﻿using System.Collections.Generic;

namespace Builder.HouseComponents
{
    internal class House
    {
        public Foundation Foundation;
        public List<Wall> Walls;
        public List<Door> Doors;
        public List<Window> Windows;
        public Paint Paint;
        public Roof Roof;
        public Floor Floor;
        public Wallpaper Wallpaper;
        public override string ToString()
        {
            return
                $"Foundation = {Foundation is {}}\nWalls = {Walls is { }}\nDoors = {Doors is { }}\nWindows = {Windows is { }}\n" +
                $"Paint = {Paint is { }}\nRoof = {Roof is { }}\nFloor = {Floor is { }}\nWallpaper = {Wallpaper is { }}";
        }
    }
}
