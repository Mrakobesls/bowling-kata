﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Builder
{
    internal enum States
    {
        Foundation = 0,
        Walls = 1,
        Doors = 2,
        Windows = 3,
        Paint = 4,
        Roof = 5,
        Floor = 6,
        Wallpaper = 7
    }
}
