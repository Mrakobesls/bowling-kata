﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Builder.HouseComponents;
using Builder.Workers;

namespace Builder
{
    internal class Director
    {
        private List<IBuilder> _builders;
        
        private House _currentHouse;
        public House CurrentHouse
        {
            private set => _currentHouse = value;
            get
            {
                if (_state.IsBuildReady())
                    return _currentHouse;
                throw new Exception("House is not ready!");
            }
        }

        private IBuilder _currentBuilder;
        private readonly BuildState _state = new BuildState();

        public Director(List<IBuilder> builders)
        {
            UpdateWorkers(builders);
        }

        public void UpdateWorkers(List<IBuilder> builders)
        {
            if (builders.Count == 0) throw new Exception("We need more builders");
            _builders = builders;
            _currentBuilder = _builders.First();
            _currentHouse = new House();
        }
        public void Reset()
        {
            foreach (var item in _builders)
            {
                item.Reset();
            }
            _state.Reset();
        }
        public void BuildFoundation()
        {
            TryBuild(States.Foundation, () => _currentBuilder.BuildFoundation());
        }

        public void BuildWalls(int count)
        {
            TryBuild(States.Walls, () => _currentBuilder.BuildWalls(count));
        }

        public void BuildDoors(int count)
        {
            TryBuild(States.Doors, () => _currentBuilder.BuildDoors(count));
        }

        public void BuildWindows(int count)
        {
            TryBuild(States.Windows, () => _currentBuilder.BuildWindows(count));
        }

        public void BuildRoof()
        {
            TryBuild(States.Roof, () => _currentBuilder.BuildRoof());
        }

        public void PaintHouse()
        {
            TryBuild(States.Paint, () => _currentBuilder.PaintHouse());
        }

        public void MakeFloor()
        {
            TryBuild(States.Floor, () => _currentBuilder.MakeFloor());
        }

        public void PasteWallpaper()
        {
            TryBuild(States.Wallpaper, () => _currentBuilder.PasteWallpaper());
        }
        
        private void TryBuild(States state, Func<House> builderAction)
        {
            if (!_state.IsPossibleToBuild(state)) throw new Exception("Wrong sequence");
            ChooseBuilder(state);
            CurrentHouse = builderAction();
            _state.MarkAsDone(state);
        }
        private void ChooseBuilder(States state)
        {
            IBuilder temp = null;
            foreach (var builder in _builders)
            {
                if (temp is null)
                    temp = builder;
                else if (builder.GetPerfomance(state) > temp.GetPerfomance(state))
                    temp = builder;
            }

            if (ReferenceEquals(_currentBuilder, temp)) return;
            _currentBuilder = temp;
            _currentBuilder?.SetHouse(_currentHouse);
        }
    }
}
